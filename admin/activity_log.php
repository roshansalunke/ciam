<?php
    session_start();
    require("../vendor/autoload.php");
    superadmin_authorize();
    include("../common/sidebar.php");
    include("../common/header.php");

    include("activitylog/listing.php");

    //echo "Hello world";
    include("../common/footer.php");
?>