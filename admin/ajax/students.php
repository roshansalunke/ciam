<?php

session_start();
require_once("../../vendor/autoload.php");
superadmin_authorize();

//admin_authorize();
$conn = getConn();
$params = $_REQUEST;

/** search logic **/

$search = isset($params['search']['value'])  && $params['search']['value'] != '' ? $params['search']['value'] : '';
$where = '';

  if($search !== '')
	  $where = " name LIKE '$search%' OR branch LIKE '$search%' OR course_name LIKE '$search%'";
  else
	  $where = " 1 = 1 ";
/** search logic end **/

/**    **/
$sort_columns = ['name', 'branch', 'course_name'];
$sort_column_index = $params['order'][0]['column'];
$sort_order = isset($params['order'][0]['dir']) && $params['order'][0]['dir'] != 1 ? $params['order'][0]['dir'] : 'desc';

$order_by = " $sort_columns[$sort_column_index] $sort_order";

/**   **/

$offset = $params['start'];
$limit = $params['length'];

$username = $_SESSION['username'];
$name = ucwords($username);

// echo $name;
$ini_params = getIniParams();
$user_name = $ini_params['superadmin_user'];

// echo $user_name;
if ($username == $user_name){
  $sql = "SELECT s.id, s.name, s.branch, s.phone_no, s.whatsapp_no, s.course_name, s.status, s.username,b.branch_name, b.id as branch_id FROM students as s LEFT JOIN branches as b ON s.branch=b.id WHERE $where ORDER BY $order_by LIMIT $offset, $limit";
// print_r("=================="); 
}else{
  // $sql = "SELECT * FROM students WHERE branch = '$username' AND $where ORDER BY $order_by LIMIT $offset, $limit";
  $sql = "SELECT s.id, s.name, s.branch, s.phone_no, s.whatsapp_no, s.course_name, s.status, s.username, b.branch_name, b.id as branch_id,a.username, a.branch_id FROM students as s LEFT JOIN branches as b ON s.branch=b.id LEFT JOIN admin as a ON s.branch = a.branch_id WHERE a.username = '$username' AND $where ORDER BY $order_by LIMIT $offset, $limit";
}

// echo $sql;
// exit;
// print_r("==================");
// exit;
// $sql = "SELECT * FROM students WHERE $where ORDER BY $order_by LIMIT $offset, $limit";
//$sql = "SELECT * FROM ciam";
// error_log("=====SQL:  $sql ===========\n\n");
//$_SESSION['last_query']= $sql;
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$sites = $stmt->fetchAll();
$count_sql = "SELECT COUNT(*) FROM students WHERE $where";
$result = $conn->prepare($count_sql);
$result->execute();
$totalRecords = $result->fetchColumn();
//$totalRecords = count($sites); 
/** PAGINATION COUNT [Stop] */

$conn = null;

$json_data = array(
    "draw"            => intval( $params['draw'] ),
    "recordsTotal"    => intval( $totalRecords ),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $sites,
    "sql"		=> $sql

);

echo json_encode($json_data);
?>
