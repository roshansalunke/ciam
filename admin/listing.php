<!-- https://datatables.net/examples/server_side/simple.html -->
<!-- https://datatables.net/manual/ajax -->
<!-- https://datatables.net/manual/ajax#Column-data-points -->
<!-- https://stackoverflow.com/questions/64526856/how-to-add-edit-delete-buttons-in-each-row-of-datatable -->
<?php 
// require("../vendor/autoload.php");

// superadmin_authorize();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">

    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.6.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <title>Document</title>
</head>
<body>
<div style="float:right; margin:5px;">
    <a class="btn btn-primary" href="add_form.php" style="margin: 10px" title="Add Student"><i class="fa fa-plus" aria-hidden="true"></i>
</a>
    <a class="btn btn-primary" href="./import_form.php"><i class="fa fa-upload" aria-hidden="true" title="Import"></i></a>&nbsp;
    <a class="btn btn-primary" href="export.php" style="margin: 10px" title="Export"><i class="fa fa-download" aria-hidden="true"></i>
    </a>
</div>
<table id="example" class="display table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Branch</th>
                <th>Phone Number</th>
                <th>Whatsapp Number</th>
                <th>Course Name</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</body>
<!-- Modal Start-->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="model-title-studentname" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="model-title-studentname"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"> 
                    <img src="" alt="Avatar" id="student-img" style="width:100px ;border-radius: 50%; margin-left: 10px"/>
                </div>
                <div class="modal-body">
                    <div>EmailId: <span id="student-emailid"></span></div>
                </div>

                <div class="modal-body">
                    <div>Address: <span id="student-address"></span></div>
                </div>
                <div class="modal-body">
                    <div>DOB:  <span id="student-DOB"></span></div>
                </div>
                <div class="modal-body">
                    <div>Joining Date:  <span id="student-JD"></span></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<!-- Modal End-->
<script>
    $(document).ready(function () {
   var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'ajax/students.php',
        columns: [
            { data: null},
            { data: 'name'},
            { data: 'branch_name'},
            { data: 'phone_no'},
            { data: 'whatsapp_no'},
            { data: 'course_name'},
            { data: 'id',
                render: (data,type,row) => {
                    //          console.log({data,type,row})
                   return  `<div class="form-check form-switch toggle-switch">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="toggleSwitch2"
                      checked
                      style="width:50px"
                    />
                  </div>`
                 }
            
            },
            { 
                data: 'id',
                render: (data,type,row) => {
                    //          console.log({data,type,row})
                   return `<a class="btn btn-primary" href='edit_form.php?id=${data}'><i class="fa fa-edit" aria-hidden="true"></i></a>  <a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete ${row['name']} record?');" href='ajax/delete.php?id=${data}'><i class="fa fa-trash" aria-hidden="true"></i></a> <button onclick="getStudentsDetails(${data})" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-eye" aria-hidden="true"></i></button>`;
                 }
            }
        ],
        columnDefs: [
            { orderable: false, targets: [-1,0,3,4,5,6] },
            {
                "defaultContent": "-",
                "targets": "_all"
            }],
            order: [[0, 'asc']],
});

table.on( 'order.dt search.dt processing.dt page.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
        var info = table.page.info();
        var page = info.page+1;
        if (page >'1') {
            hal = (page-1) *10;  // u can change this value of ur page
            cell.innerHTML = hal+i+1;
        }
    } );
}).draw();
  });


function getStudentsDetails(studentid) {
    var studentname = event.target.value;
    fetch(`ajax/get_details.php?id=${studentid}`)
        .then(res => res.json())
        .then(result => {
            document.getElementById("model-title-studentname").innerHTML = result['name'];
            document.getElementById("student-img").src = "../upload_std_img/"+result['student_img'];
            document.getElementById("student-emailid").innerHTML = result['emailId'];
            document.getElementById("student-address").innerHTML = result['address'];
            document.getElementById("student-DOB").innerHTML = result['DOB'];
            document.getElementById("student-JD").innerHTML = result['created_at'];
            
        })

}

  $(function(){
    $('#chkToggle').bootstrapToggle();
  });
</script>
</html>
