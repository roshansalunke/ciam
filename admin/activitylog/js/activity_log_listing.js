
$(document).ready(function() {
    $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'activitylog/ajax/get_listing.php',
        columns: [
            {data: 'SrNo',
       render: function (data, type, row, meta) {
            return meta.row + 1;
       		}
	    },
            {
                data: 'ipaddr'
            },
	     {
                data: 'user_name'
            },
	     {
                data: 'amsg'
            },
	     {
                data: 'adate'
            },
            {   width: '250px',
                data: 'useragt',
                render: (data, type, row) => {
                        if(data.length >= 30)
                        {
                                data = data.slice(0,30) + '...';
                                return data;
                        }
                        else
                        {
                                return data;
                        }

                }

            },
              ],
        columnDefs: [{
                orderable: false,
                targets: [0, 1, 2, 3, -1]
            },
            {
                "defaultContent": "-",
                "targets": "_all"
            }
        ],
        order: [0, 1, 2, 3, 4, 5],
    });
});

