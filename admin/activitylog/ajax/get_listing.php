<?php

session_start();
require_once("../../../vendor/autoload.php");
//admin_authorize();
$conn = getConn();


$params = $_REQUEST;

/** SEARCH LOGIC [Start] */
$search = isset($params['search']['value'])  && $params['search']['value'] != '' ? $params['search']['value'] : '';

$where = '';
if($search !== '')
    $where = "user_name LIKE '$search%' OR amsg LIKE '$search%' OR adate LIKE '$search%'";
else
    $where = " 1 = 1 ";
/** SEARCH LOGIC [Stop] */



/** SORTING LOGIC [Start] */
$sort_columns = ['adate']; //only this needs to change
$sort_column_index = $params['order'][0]['column'];

$sort_order = isset($params['order'][0]['dir']) && $params['order'][0]['dir'] != 1 ? $params['order'][0]['dir'] : 'desc';

$order_by = " $sort_columns[$sort_column_index] $sort_order";
/** SORTING LOGIC [Stop] */


/** PAGINATION LOGIC [Start] */
$offset = $params['start'];
$limit = $params['length'];
/** PAGINATION LOGIC [Start] */

$sql = "SELECT * FROM auditlogs WHERE $where ORDER BY $order_by  LIMIT $offset, $limit";
// error_log("=====SQL:  $sql ===========\n\n");

$_SESSION['last_query']= $sql;

$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$plots = $stmt->fetchAll();

/** PAGINATION COUNT [Start] */

$count_sql = "SELECT COUNT(*) FROM auditlogs WHERE $where";
$result = $conn->prepare($count_sql);
$result->execute();
$totalRecords = $result->fetchColumn();
/** PAGINATION COUNT [Stop] */

$conn = null;

$json_data = array(
    "draw"            => intval( $params['draw'] ),
    "recordsTotal"    => intval( $totalRecords ),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $plots,
    // "count_sql"      => $count_sql,
    "sql"               => $sql,
);

echo json_encode($json_data);