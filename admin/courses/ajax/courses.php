<?php

//session_start();
require_once("../../../vendor/autoload.php");
//admin_authorize();
$conn = getConn();
$params = $_REQUEST;

/** search logic **/

$search = isset($params['search']['value'])  && $params['search']['value'] != '' ? $params['search']['value'] : '';
$where = '';

  if($search !== '')
	  $where = " course_name LIKE '$search%' OR fees LIKE '$search%' OR description LIKE '$search%'";
  else
	  $where = " 1 = 1 ";
/** search logic end **/

/**    **/
$sort_columns = ['course_name', 'fees', 'description'];
$sort_column_index = $params['order'][0]['column'];
$sort_order = isset($params['order'][0]['dir']) && $params['order'][0]['dir'] != 1 ? $params['order'][0]['dir'] : 'desc';

$order_by = " $sort_columns[$sort_column_index] $sort_order";

/**   **/

$offset = $params['start'];
$limit = $params['length'];

$sql = "SELECT * FROM courses WHERE $where ORDER BY $order_by LIMIT $offset, $limit";
//$sql = "SELECT * FROM ciam";
// error_log("=====SQL:  $sql ===========\n\n");
//$_SESSION['last_query']= $sql;
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$sites = $stmt->fetchAll();
$count_sql = "SELECT COUNT(*) FROM courses WHERE $where";
$result = $conn->prepare($count_sql);
$result->execute();
$totalRecords = $result->fetchColumn();
//$totalRecords = count($sites); 
/** PAGINATION COUNT [Stop] */

$conn = null;

$json_data = array(
    "draw"            => intval( $params['draw'] ),
    "recordsTotal"    => intval( $totalRecords ),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $sites,
    "sql"		=> $sql

);

echo json_encode($json_data);
?>
