<!-- https://datatables.net/examples/server_side/simple.html -->
<!-- https://datatables.net/manual/ajax -->
<!-- https://datatables.net/manual/ajax#Column-data-points -->
<!-- https://stackoverflow.com/questions/64526856/how-to-add-edit-delete-buttons-in-each-row-of-datatable -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">

    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.6.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <title>Document</title>
</head>
<body>
<div style="float:right; margin:5px;">
    <a class="btn btn-primary" href="addcourses_form.php" style="margin: 10px" title="Add Course "><i class="fa fa-plus" aria-hidden="true"></i>
</a>
    <a class="btn btn-primary" href="./import_course_form.php"><i class="fa fa-upload" aria-hidden="true" title="Import"></i></a>&nbsp;
    <a class="btn btn-primary" href="./export_course.php" style="margin: 10px" title="Export"><i class="fa fa-download" aria-hidden="true"></i>
    </a>
</div>
<table id="example" class="display table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Course Name</th>
                <th>Fees</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</body>
<script>
    $(document).ready(function () {
   var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'courses/ajax/courses.php',
        columns: [
            { data: null},
            { data: 'course_name'},
            { data: 'fees'},
            { data: 'description'},
            { 
                data: 'cid',
                render: (data,type,row) => {
                    //          console.log({data,type,row})
                   return `<a class="btn btn-primary" href='editcourses_form.php?cid=${data}'><i class="fa fa-edit" aria-hidden="true"></i></a>  <a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete ${row['course_name']} record?');" href='deletecourses.php?cid=${data}'><i class="fa fa-trash" aria-hidden="true"></i></a>`;
                 }
            }
        ],
        columnDefs: [
            { orderable: false, targets: [-1,0,3] },
            {
                "defaultContent": "-",
                "targets": "_all"
            }],
            order: [[0, 'asc']],
});

table.on( 'order.dt search.dt processing.dt page.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
        var info = table.page.info();
        var page = info.page+1;
        if (page >'1') {
            hal = (page-1) *10;  // u can change this value of ur page
            cell.innerHTML = hal+i+1;
        }
    } );
}).draw();
  });


// function getStudentsDetails(studentid) {
//     var studentname = event.target.value;
//     fetch(`admin/ajax/get_details.php?id=${studentid}`)
//         .then(res => res.json())
//         .then(result => {
//             document.getElementById("model-title-studentname").innerHTML = result['name'];
//             document.getElementById("student-emailid").innerHTML = result['emailId'];
//             document.getElementById("student-address").innerHTML = result['address'];
//             document.getElementById("student-DOB").innerHTML = result['DOB'];
//         })

// }

//   $(function(){
//     $('#chkToggle').bootstrapToggle();
//   });
</script>
</html>
