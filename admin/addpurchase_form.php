<?php

include("../common/sidebar.php");
include("../common/header.php");
?>
<div class="col-lg-6" style ="margin-top: 20px; width: 100%">
  <form action = "./addpurchase_action.php" method="post" enctype="multipart/form-data">
    <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
      <!-- input style start -->
        <h4 class="mb-25">Add Purchase details</h4>
        
        <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
          <div class="input-style-1">
            <label>Items  <span class="required">*</span></label>
            <input type="text" name="items" placeholder="items" required onkeyup="checkDuplicateAdd(event)"/>
            <div id = "validation-msg" style = "color:red"></div>
          </div>

          <div class="input-style-1">
            <label>Branch  <span class="required">*</span></label>
            <input type="text" name="branch" placeholder="branch" required/>
          </div>
        
          
          <div class="input-style-1">
            <label>Cost  <span class="required">*</span></label>
            <input type="text"  name="cost" placeholder="cost" required/>
          </div>
        
          
          <div class="input-style-1">
            <label>Comments</label>
            <input type="text" name="comments" placeholder="comments"/>
          </div>

          <div class="input-style-1">
            <label>Upload Bill</label>
            <input type="file" name="file_name" placeholder="file_name"/>
          </div>
          
          <div class="input-style-1">
            <label>Date </label>
            <input type="text" name="date" placeholder="date" />
          </div>
        </div>
        <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
            <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
            <input class="main-btn primary-btn btn-hover" id="submit-btn" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
            <a href= "addpurchase_form.php?id=<?php echo $p_id; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
            <a href="purchase.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
        </div>
      
    </div>    
  </form>
</div>
<script src="students.js"></script>
<?php
include("../common/footer.php");

?>