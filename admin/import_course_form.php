<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');
?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%;">
              <form action="import_course_action.php" method="post" enctype="multipart/form-data">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Import CSV File </h4>
                  <input type="hidden" name="id" value="<?php echo $student['id']; ?>" />
                  
                  <a href="samplecourse_file.csv">Download Sample File</a>
                
				    <div class="mb-3"><br>
                                    <label for="sitename" class="form-label">Upload File:</label>
                                <input class="form-control" type="file" id="import_file" name="csvfile" autofocus>
                                    <div id="validation-msg"></div>
                                </div>
              
                    <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
                    <a href= "courses.php" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
                    </br></br></br> 
                </div>  
                </div>

                
</form>
    </div>

<?php    
include('common/footer.php');
?>
