<?php
session_start();

require("../vendor/autoload.php");
superadmin_authorize();

include("../common/sidebar.php");
include("../common/header.php");
include("listing.php");

include("../common/footer.php");
?>
