<?php

require_once("../vendor/autoload.php");
$conn= getConn();
//$p_id = $_REQUEST['p_id'];
$items = $_REQUEST['items'];
$branch = $_REQUEST['branch'];
$cost = $_REQUEST['cost'];
$comments = $_REQUEST['comments'];
$file_name = $_REQUEST['file_name'];
$date = $_REQUEST['date'];
//$status = $_REQUEST['status'];
//$status = "1";
// $year_of_passing = $_REQUEST['year_of_passing'];
// $college = $_REQUEST['college'];
// $date = $_REQUEST['date'];
//$dob = $_REQUEST['dob'];

/** uploade image */

$target_dir = "upload_pch_img/";
$target_file = $target_dir . basename($_FILES["file_name"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file_name"]["tmp_name"]);
    if($check !== false) {
      echo "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
      
    } else {
      echo "File is not an image.";
      $uploadOk = 0;
    }
  }

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
  }
  
  // Check file size
  if ($_FILES["file_name"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
  }
  
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
  }
  
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
  // if everything is ok, try to upload file
  } else {
    if (move_uploaded_file($_FILES["file_name"]["tmp_name"], $target_file)) {
      echo "The file ". htmlspecialchars( basename( $_FILES["file_name"]["name"])). " has been uploaded.";
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
  }
  
  $file_name = basename( $_FILES["file_name"]["name"]);

/** uploade image complete*/


$sql= "INSERT INTO `purchase` (`p_id`, `items`, `branch`, `cost`, `comments`,`file_name`, `date`) 
VALUES (NULL, '$items', '$branch', '$cost', '$comments' ,'$file_name',NOW())";
$stmt = $conn->prepare($sql);
$stmt->execute(); 

header("Location: purchase.php");
?>
