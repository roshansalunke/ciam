<?php

include("../common/sidebar.php");
include("../common/header.php");
?>
<div class="col-lg-6" style ="margin-top: 20px; width: 100%">
  <form action = "./addbranches_action.php" method="post" enctype="multipart/form-data">
    <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
      <!-- input style start -->
        <h4 class="mb-25">Add Branches</h4>
        
        <div style="display: grid; grid-template-columns: repeat(1, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
          <div class="input-style-1">
            <label>Branch Name <span class="required">*</span></label>
            <input type="text" name="branch_name" placeholder="Branch Name" required/>
            <div id = "validation-msg" style = "color:red"></div>
          </div>

        <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
            <input class="main-btn primary-btn btn-hover" id="submit-btn" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
            <a href= "addcourses_form.php?bid=<?php echo $bid; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
            <a href="branches.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
        </div>
      
    </div>    
  </form>
</div>
<script src="students.js"></script>
<?php
include("../common/footer.php");

?>