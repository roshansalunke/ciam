<?php
session_start();

require("../vendor/autoload.php");
superadmin_authorize();

include("../common/sidebar.php");
include("../common/header.php");

$username = $_SESSION['username'];
$name = ucwords($username);
echo "<div style='text-align:center; margin-top: 20px'><h3>Hello {$name}</h3></div>";

include("../common/footer.php");
?>
