<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');


$conn = getConn();
$id = $_REQUEST['bid'];

$sql = "SELECT * FROM branches WHERE id = $id";
// error_log("==============SQL: $sql==============\n\n");

$result = $conn->prepare($sql);                                     
$result->execute();
$stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
$branch=$result->fetch();

// print_r($course);


    ?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%">
              <form action="./editbranches_action.php">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Edit Course Details</h4>
                  <input type="hidden" name="id" value="<?php echo $branch['id']; ?>" />
                  
                  <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
                  <div class="input-style-1" >
                    <label>Branch Name</label>
                    <input type="text" name="branch_name" value="<?php echo $branch['branch_name'];?>" required onkeyup="editValidation(event)" autofocus autocomplete="off"/>
                    <div id = "validation-msg" style = "color:red"></div>
                  </div>
                
                  <!-- <div class="input-style-1">
                    <label>Fees</label>
                    <input type="text" value="<?php echo $course['fees']; ?>" name="fees" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Description</label>
                    <input type="text" value="<?php echo $course['description']; ?>" name="description" autocomplete="off"/>
                  </div> -->
                  </div>
                  <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"  />
                    <a href= "editbranches_form.php?id=<?php echo $id; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
                    <a href="branches.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
                  </div>  
                </div>
         
</form>
    </div>

    <script src="students.js"></script>

<?php    
include('../common/footer.php');
?>
