<?php
//session_start();
require_once("../vendor/autoload.php");
//admin_authorize();

$conn = getConn();
$msg = 'Sites Exported';

    $sql = "SELECT lid,name,contact,course FROM leads";
	//$sql = $_SESSION['last_query'];
	//error_log("==============SQL: $sql==============\n\n");

    $result = $conn->prepare($sql); 
    $result->execute();
    $stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
    $site=$result->fetchAll(); 

function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    
    $f = fopen('php://output', 'w');
    
    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}
//for heading
array_unshift($site, ['ID', 'Name', 'Contact', 'Course Name']);

array_to_csv_download($site, "export.csv", ",");

//activity_log($msg);

?>

