<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');


$conn = getConn();
$lid = $_REQUEST['lid'];

$sql = "SELECT * FROM leads WHERE lid = $lid";
// error_log("==============SQL: $sql==============\n\n");

$result = $conn->prepare($sql);                                     
$result->execute();
$stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
$leads=$result->fetch();

// print_r($course);


    ?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%">
              <form action="./editleads_action.php">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Edit Leads Details</h4>
                  <input type="hidden" name="lid" value="<?php echo $leads['lid']; ?>" />
                  
                  <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
                  <div class="input-style-1" >
                    <label>Name</label>
                    <input type="text" name="name" value="<?php echo $leads['name'];?>" required onkeyup="editValidation(event)" autofocus autocomplete="off"/>
                    <div id = "validation-msg" style = "color:red"></div>
                  </div>
                
                  <div class="input-style-1">
                    <label>Contact</label>
                    <input type="text" value="<?php echo $leads['contact']; ?>" name="fees" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Course</label>
                    <input type="text" value="<?php echo $leads['course']; ?>" name="description" autocomplete="off"/>
                  </div>
                  </div>
                  <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"  />
                    <a href= "editleads_form.php?lid=<?php echo $lid; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
                    <a href="leads.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
                  </div>  
                </div>
         
</form>
    </div>

    <script src="students.js"></script>

<?php    
include('../common/footer.php');
?>
