<?php

require("../vendor/autoload.php");
include("../common/sidebar.php");
include("../common/header.php");

$conn = getConn();

$sql = "select * from branches";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$branches = $stmt->fetchAll();

?>
<div class="col-lg-6" style ="margin-top: 20px; width: 100%">
  <form action = "./addusers_action.php" method="post" enctype="multipart/form-data">
    <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
      <!-- input style start -->
        <h4 class="mb-25">Add Users details</h4>
        
        <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
          <div class="input-style-1">
            <label>Branch Name <span class="required">*</span></label>
            <!-- <input type="text" name="course_name" placeholder="Course Name" required/>
            <div id = "validation-msg" style = "color:red"></div> -->
            <select name="branch_name" id="b_id">
              <option id="id">---Select Branch---</option>
              <?php
                foreach($branches as $row)
                {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['branch_name']; ?></option>
                  <?php } ?>
            </select>
          </div>

          <div class="input-style-1">
            <label>User Name <span class="required">*</span></label>
            <input type="text" name="username" placeholder="username" required/>
          </div>
        
          <div class="input-style-1">
            <label>Password</label>
            <input type="text" name="password" placeholder="password"/>
          </div> 
        </div>
        <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
            <input class="main-btn primary-btn btn-hover" id="submit-btn" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
            <a href= "addcourses_form.php?id=<?php echo $cid; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
            <a href="courses.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
        </div>
      
    </div>    
  </form>
</div>
<script src="students.js"></script>
<?php
include("../common/footer.php");

?>