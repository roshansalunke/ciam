function checkDuplicateAdd(event){
    var student_name = event.target.value;
    fetch(`./admin/ajax/check_duplicate_add.php?student_name=${student_name}`)
    .then(res => res.json())
    .then(result => {
        console.log(result['data']);
        if(result['data'] !== false) {
            document.getElementById("validation-msg").innerHTML = `*Student already exists`;
            document.getElementById("submit-btn").disabled = true;
        }

           else {
            document.getElementById("validation-msg").innerHTML = ``
            document.getElementById("submit-btn").disabled = false;
        }

    });
}

function checkDuplicateUsername(event){
    var student_username = event.target.value;
    fetch(`./admin/ajax/check_duplicate_username.php?student_username=${student_username}`)
    .then(res => res.json())
    .then(result => {
        console.log(result['data']);
        if(result['data'] !== false) {
            document.getElementById("Username_validation-msg").innerHTML = `*User Name already exists`;
            document.getElementById("submit-btn").disabled = true;
        }

           else {
            document.getElementById("Username_validation-msg").innerHTML = ``
            document.getElementById("submit-btn").disabled = false;
        }

    });
}


function editValidation(){

    var name = event.target.value;

    fetch(`./admin/ajax/check_duplicate_edit.php?student_name=${name}`)
    .then(res => res.json())
    .then(result => {

        if(result['data'] !== false) {
            document.getElementById("validation-msg").innerHTML = `*Student already exists`;
            document.getElementById("submit-btn").disabled = true;
        }

           else {
            document.getElementById("validation-msg").innerHTML = ``
            document.getElementById("submit-btn").disabled = false;
        }
    })
}