<?php

include("../common/sidebar.php");
include("../common/header.php");
?>
<div class="col-lg-6" style ="margin-top: 20px; width: 100%">
  <form action = "./addleads_action.php" method="post" enctype="multipart/form-data">
    <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
      <!-- input style start -->
        <h4 class="mb-25">Add Leads details</h4>
        
        <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
          <div class="input-style-1">
            <label>Full Name  <span class="required">*</span></label>
            <input type="text" name="name" placeholder="Full Name" required onkeyup="checkDuplicateAdd(event)"/>
            <div id = "validation-msg" style = "color:red"></div>
          </div>

          <div class="input-style-1">
            <label>Gmail</label>
            <input type="text" name="gmail" placeholder="gmail"/>
          </div>
        
          
          <div class="input-style-1">
            <label>Contact Number<span class="required">*</span></label>
            <input type="text"  name="contact" placeholder="contact_no" required/>
          </div>
        
          
         <div class="input-style-1">
            <label>Course  <span class="required">*</span></label>
            <input type="text" name="course" placeholder="course" required/>
          </div> 
          
          <div class="input-style-1">
            <label>Graduation</label>
            <input type="text" name="graduation" placeholder="graduation"/>
          </div>
          
          <div class="input-style-1">
            <label>Year of passing<span class="required">*</span></label>
            <input type="text" name="year_of_passing" placeholder="yyyy" required onkeyup="checkDuplicateUsername(event)"/>
          </div>

          <div class="input-style-1">
            <label>College</label>
            <input type="text" name="college" placeholder="college"/>
          </div>
        </div>
        <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
            <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
            <input class="main-btn primary-btn btn-hover" id="submit-btn" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
            <a href= "add_form.php?id=<?php echo $lid; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
            <a href="leads.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
        </div>
      
    </div>    
  </form>
</div>
<script src="students.js"></script>
<?php
include("../common/footer.php");

?>