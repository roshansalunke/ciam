<?php

require("../vendor/autoload.php");


include("../common/sidebar.php");
include("../common/header.php");

$conn = getConn();

$sql1 = "select * from courses";
$stmt1 = $conn->prepare($sql1);
$stmt1->execute();
$result1 = $stmt1->setFetchMode(PDO::FETCH_ASSOC);
$course = $stmt1->fetchAll();

$sql2 = "select * from branches";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute();
$result2 = $stmt2->setFetchMode(PDO::FETCH_ASSOC);
$branches = $stmt2->fetchAll();

?>
<div class="col-lg-6" style ="margin-top: 20px; width: 100%">
  <form action = "./add_action.php" method="post" enctype="multipart/form-data">
    <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
      <!-- input style start -->
        <h4 class="mb-25">Add Student details</h4>
        
        <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
          <div class="input-style-1">
            <label>Full Name  <span class="required">*</span></label>
            <input type="text" name="name" placeholder="Full Name" required onkeyup="checkDuplicateAdd(event)"/>
            <div id = "validation-msg" style = "color:red"></div>
          </div>

          <!-- <div class="input-style-1">
            <label>Branch  <span class="required">*</span></label>
            <input type="text" name="branch" placeholder="branch" required/>
          </div> -->
        
          <div class="input-style-1">
            <label>Branch<span class="required">*</span></label>
            <select name="branch" id="b_id">
              <option id="b_id">---Select branches---</option>
              <?php
                foreach($branches as $row)
                {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['branch_name']; ?></option>
                  <?php } ?>
            </select>
          </div>
          
          <div class="input-style-1">
            <label>Phone Number</label>
            <input type="text"  name="phone_no" placeholder="phone_no"/>
          </div>
        
          
          <div class="input-style-1">
            <label>Whatsapp Number  <span class="required">*</span></label>
            <input type="text" name="whatsapp_no" placeholder="whatsapp_no" required/>
          </div>
        
          
          <div class="input-style-1">
            <label>Course Name  <span class="required">*</span></label>
            <select name="course_name" id="c_id">
              <option id="c_id">---Select Course---</option>
              <?php
                foreach($course as $row)
                {
                  ?>
                  <option value="<?php echo $row['course_name']; ?>"><?php echo $row['course_name']; ?></option>
                  <?php } ?>
            </select>
          </div>
          
        <!--- <div class="input-style-1">
            <label>Status</label>
            <input type="text" name= "status" placeholder="status" />
          </div>--->
          <div class="input-style-1">
            <label>User Name  <span class="required">*</span></label>
            <input type="text" name="username" placeholder="username" required onkeyup="checkDuplicateUsername(event)"/>
            <div id = "Username_validation-msg" style = "color:red"></div>
          </div>

          <div class="input-style-1">
            <label>Address</label>
            <input type="text" name="address" placeholder="address"/>
          </div>
          
          <div class="input-style-1">
            <label>Email Id  <span class="required">*</span></label>
            <input type="text" name="email_id" placeholder="email id" required/>
          </div>
          
          <div class="input-style-1">
            <label>DOB  <span class="required">*</span></label>
            <input type="text" name="dob" placeholder="yyyy-mm-dd" required/>
          </div>
          
          <div class="input-style-1">
            <label>upload Image  <span class="required">*</span></label>
            <input type="file" name="fileToUpload" id="fileToUpload">
          </div>


        </div>
        <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
            <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
            <input class="main-btn primary-btn btn-hover" id="submit-btn" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"/>
            <a href= "add_form.php?id=<?php echo $id; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
            <a href="student.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
        </div>
      
    </div>    
  </form>
</div>
<script src="students.js"></script>
<?php
include("../common/footer.php");

?>