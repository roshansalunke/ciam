<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');

$conn = getConn();
$id = $_REQUEST['id'];

//$sql = "SELECT studentsDetails.emailId, studentsDetails.address, students.name, students.branch, students.whatsapp_no, students.course_name, students.phone_no from students LEFT JOIN studentsDetails ON students.id=studentsDetails.id WHERE students.id = '$id' ORDER BY students.id";
$sql = "SELECT studentsDetails.emailId, studentsDetails.address, studentsDetails.emailid, students.id,students.name, students.branch, students.phone_no, students.whatsapp_no, students.course_name, students.username, branches.branch_name, branches.id as branch_id FROM students LEFT JOIN branches ON students.branch=branches.id LEFT JOIN studentsDetails ON students.id=studentsDetails.id WHERE students.id = '$id' ORDER BY students.id";
//$sql = "SELECT * FROM students WHERE id = $id";

// error_log("==============SQL: $sql==============\n\n");

$result = $conn->prepare($sql);
$result->execute();
$stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
$student=$result->fetch();


$sql1 = "select * from courses";
$stmt1 = $conn->prepare($sql1);
$stmt1->execute();
$result1 = $stmt1->setFetchMode(PDO::FETCH_ASSOC);
$course = $stmt1->fetchAll();

$sql2 = "select * from branches";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute();
$result2 = $stmt2->setFetchMode(PDO::FETCH_ASSOC);
$branches = $stmt2->fetchAll();


// print_r($student);


    ?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%">
              <form action="./edit_action.php">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Edit Student Details</h4>
                  <input type="hidden" name="id" value="<?php echo $student['id']; ?>" />
                   
                  <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
                  <div class="input-style-1" >
                    <label>Full Name</label>
                    <input type="text" name="name" value="<?php echo $student['name'];?>" required onkeyup="editValidation(event)" autofocus autocomplete="off"/>
                    <div id = "validation-msg" style = "color:red"></div>
                  </div>
                
                  <!-- <div class="input-style-1">
                    <label>Branch</label>
                    <input type="text" value="<?php echo $student['branch']; ?>" name="branch" autocomplete="off"/>
                  </div> -->

                  <div class="input-style-1">
                    <label>Branch<span class="required">*</span></label>
                    <select name="branch" id="b_id">
                      <option value="<?php echo $student['branch_id']; ?>"><?php echo $student['branch_name']; ?></option>
                      <?php
                        foreach($branches as $row)
                        {
                          ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['branch_name']; ?></option>
                          <?php } ?>
                    </select>
                  </div>

                  <div class="input-style-1">
                    <label>Phone Number</label>
                    <input type="text" value="<?php echo $student['phone_no']; ?>" name="phone_no" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Whatsapp Number</label>
                    <input type="text" value="<?php echo $student['whatsapp_no']; ?>" name="whatsapp_no" required autocomplete="off"/>
                  </div>

                  <!-- <div class="input-style-1">
                    <label>Course Name</label>
                    <input type="text" value="<?php echo $student['course_name']; ?>" name="course_name" autocomplete="off"/>
                  </div> -->

                  <div class="input-style-1">
                    <label>Course Name  <span class="required">*</span></label>
                    <select name="course_name" id="c_id">
                      <option id="c_id"><?php echo $student['course_name']; ?></option>
                      <?php
                        foreach($course as $row)
                        {
                          ?>
                          <option value="<?php echo $row['course_name']; ?>"><?php echo $row['course_name']; ?></option>
                          <?php } ?>
                    </select>
                  </div>

                  <div class="input-style-1">
                    <label>User Name</label>
                    <input type="text" value="<?php echo $student['username']; ?>" name="username" autocomplete="off"/>
                  </div> 

                  <div class="input-style-1">
                    <label>Email Id</label>
                    <input type="text" value="<?php echo $student['emailid']; ?>" name="email_id" autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Address</label>
                    <input type="text" value="<?php echo $student['address']; ?>" name="address" autocomplete="off"/>
                  </div>
        
                  <!-- <div class="input-style-1">
                    <label>Status</label>
                     <input type="text" value="<?php echo $student['status']; ?>" name="status" autocomplete="off"/>                  </div>
                  </div> -->
                  <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width: 35%; padding:10px; margin-right: 10px" value="submit"  />
                    <a href= "edit_form.php?id=<?php echo $id; ?>" class="main-btn secondary-btn btn-hover" style="width:35%; padding:8px; margin-right: 10px">Cancel</a>
                    <a href="student.php" class="main-btn dark-btn btn-hover" style="width:35%; padding:8px">Back</a>
                  </div>  
                </div>
         
</form>
    </div>

    <script src="students.js"></script>

<?php    
include('../common/footer.php');
?>
