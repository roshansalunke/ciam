<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');


$conn = getConn();
$id = $_REQUEST['id'];

// $sql = "SELECT * FROM admin WHERE id = $id";
$sql = "SELECT admin.id,admin.branch_id, admin.username, admin.password, branches.branch_name FROM admin LEFT JOIN branches ON admin.branch_id=branches.id WHERE admin.id=$id";
// error_log("==============SQL: $sql==============\n\n");

$result = $conn->prepare($sql);                                     
$result->execute();
$stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
$user=$result->fetch();

$sql2 = "select * from branches";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute();
$result2 = $stmt2->setFetchMode(PDO::FETCH_ASSOC);
$branches = $stmt2->fetchAll();

// print_r($course);


    ?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%">
              <form action="./editusers_action.php">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Edit Course Details</h4>
                  <input type="hidden" name="id" value="<?php echo $user['id']; ?>" />
                  
                  <div class="input-style-1">
                    <label>Branch<span class="required">*</span></label>
                    <select name="branch" id="b_id">
                      <option value="<?php echo $user['branch_id']; ?>"><?php echo $user['branch_name']; ?></option>
                      <?php
                        foreach($branches as $row)
                        {
                          ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['branch_name']; ?></option>
                          <?php } ?>
                    </select>
                  </div>
                
                  <div class="input-style-1">
                    <label>User Name</label>
                    <input type="text" value="<?php echo $user['username']; ?>" name="username" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Password</label>
                    <input type="text" value="<?php echo $user['password']; ?>" name="password" autocomplete="off"/>
                  </div>
                  </div>
                  <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width:10%; padding:8px; margin-right: 10px" value="submit"  />
                    <a href= "edit_form.php?id=<?php echo $id; ?>" class="main-btn secondary-btn btn-hover" style="width:10%; padding:8px; margin-right: 10px">Cancel</a>
                    <a href="users.php" class="main-btn dark-btn btn-hover" style="width:10%; padding:8px">Back</a>
                  </div>  
                </div>
         
</form>
    </div>

    <script src="students.js"></script>

<?php    
include('../common/footer.php');
?>
