<?php
require("../vendor/autoload.php");
include("../common/sidebar.php");
include('../common/header.php');

$conn = getConn();
$p_id = $_REQUEST['p_id'];

$sql = "SELECT * FROM purchase WHERE p_id = $p_id";
// error_log("==============SQL: $sql==============\n\n");

$result = $conn->prepare($sql);
$result->execute();
$stmt = $result->setFetchMode(PDO::FETCH_ASSOC);
$purchase=$result->fetch();

// print_r($student);


    ?>

    <div class="col-lg-6" style ="margin-top: 20px; width: 100%">
              <form action="./editpurchase_action.php">
                <div class="card-style mb-30" style="justify-content:center; margin: auto;width: 50%;padding: 20px;">
                  <h4 class="mb-25">Edit Purchase Details</h4>
                  <input type="hidden" name="p_id" value="<?php echo $purchase['p_id']; ?>" />
                   
                  <div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-template-rows: repeat(2, 100px);grid-column-gap: 20px;grid-row-gap: 10px; ">
                  <div class="input-style-1" >
                    <label>Items</label>
                    <input type="text" name="items" value="<?php echo $purchase['items'];?>" required onkeyup="editValidation(event)" autofocus autocomplete="off"/>
                    <div id = "validation-msg" style = "color:red"></div>
                  </div>
                
                  <div class="input-style-1">
                    <label>Branch</label>
                    <input type="text" value="<?php echo $purchase['branch']; ?>" name="branch" autocomplete="off"/>
                    </div>
                    
                  <div class="input-style-1">
                    <label>Cost</label>
                    <input type="text" value="<?php echo $purchase['cost']; ?>" name="cost" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Comments</label>
                    <input type="text" value="<?php echo $purchase['comments']; ?>" name="comments" required autocomplete="off"/>
                  </div>

                  <div class="input-style-1">
                    <label>Date</label>
                    <input type="text" value="<?php echo $purchase['date']; ?>" name="date" autocomplete="off"/>
                  </div>

                  <div style="margin-bottom: 20px;display: flex; align-items: center; justify-content: center; margin-top: 10px"> 
                    <!-- <a href="edit_action.php" class="main-btn primary-btn btn-hover" style="width:20%; padding:10px; margin-right: 10px">Submit</a> -->
                    <input class="main-btn primary-btn btn-hover" type="submit" style="width:17%; padding:8px; margin-right: 10px" value="submit"  />
                    <a href= "editpurchase_form.php?p_id=<?php echo $p_id; ?>" class="main-btn secondary-btn btn-hover" style="width:17%; padding:8px; margin-right: 10px">Cancel</a>
                    <a href="purchase.php" class="main-btn dark-btn btn-hover" style="width:17%; padding:8px">Back</a>
                  </div>  
                </div>
         
</form>
    </div>

    <script src="students.js"></script>

<?php    
include('../common/footer.php');
?>
